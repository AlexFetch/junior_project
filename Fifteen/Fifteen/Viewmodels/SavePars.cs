﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fifteen.Viewmodels
{
    public class SavePars
    {
        public string ParName { get; set; }
        public string ParValue { get; set; }
    }
}