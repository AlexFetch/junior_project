﻿var count = 0;
var pusb = 20;

$(document).ready(function () {
    for (count = 0; count < 16; count++) {
        if ($("input[name = 'butt" + count + "']").val() == "0") {
            $("input[name = 'butt" + count + "']").val("");
            $("input[name = 'butt" + count + "']").css("background-color", "white");
            break;
        }
    }
    $("input[name = 'Gamefin']").val("unfin");
    $("input[name = 'Unloadbut']").val("not");

    $("input[name = 'butt0']").click(function () { Pushed("0"); });
    $("input[name = 'butt1']").click(function () { Pushed("1"); });
    $("input[name = 'butt2']").click(function () { Pushed("2"); });
    $("input[name = 'butt3']").click(function () { Pushed("3"); });
    $("input[name = 'butt4']").click(function () { Pushed("4"); });
    $("input[name = 'butt5']").click(function () { Pushed("5"); });
    $("input[name = 'butt6']").click(function () { Pushed("6"); });
    $("input[name = 'butt7']").click(function () { Pushed("7"); });
    $("input[name = 'butt8']").click(function () { Pushed("8"); });
    $("input[name = 'butt9']").click(function () { Pushed("9"); });
    $("input[name = 'butt10']").click(function () { Pushed("10"); });
    $("input[name = 'butt11']").click(function () { Pushed("11"); });
    $("input[name = 'butt12']").click(function () { Pushed("12"); });
    $("input[name = 'butt13']").click(function () { Pushed("13"); });
    $("input[name = 'butt14']").click(function () { Pushed("14"); });
    $("input[name = 'butt15']").click(function () { Pushed("15"); });

    $("#Res").click(function () { Reset(); });
    $("#Fin").click(function () { Next(); });
});

function Pushed(pusb) {
    var color = 0;
    for (count = 0; count < 16; count++) {
        color = $("input[name = 'butt" + count + "']").css("background-color");
        if (color == "rgb(255, 255, 255)") {
            var wb = count;
            break;
        }
    }

    var pbl = $("input[name = 'butt" + pusb + "']").offset().left;
    var pbt = $("input[name = 'butt" + pusb + "']").offset().top;
    var pbv = $("input[name = 'butt" + pusb + "']").val();
    var zbl = $("input[name = 'butt" + wb + "']").offset().left;
    var zbt = $("input[name = 'butt" + wb + "']").offset().top;
    var move = false;
    if (zbl - pbl == 50 && zbt == pbt)
        move = true;
    else
        if (zbl - pbl == -50 && zbt == pbt)
            move = true;
        else
            if (zbt - pbt == 50 && zbl == pbl)
                move = true;
            else
                if (zbt - pbt == -50 && zbl == pbl)
                    move = true;
    if (move == true) {
        $("input[name = 'butt" + pusb + "']").css("background-color", "white");
        $("input[name = 'butt" + pusb + "']").val($("input[name = 'butt" + wb + "']").val());
        $("input[name = 'butt" + wb + "']").css("background-color", "#9cf527");
        $("input[name = 'butt" + wb + "']").val(pbv);
        pbv = $("input[name = 'Turns']").val();
        pbv++;
        $("input[name = 'Turns']").val(pbv);
    }
    Finish();
}

function Finish() {
    var win = false;
    for (count = 0; count < 16; count++) {
        if ($("input[name = 'butt" + count + "']").val() != count) {
            break;
        }
        if (count == 15)
            win = true;
    }
    if (win == true) {
        $("#Fin").prop('disabled', false);
        $("#Fin").css("color", "#333333");
        $("#Fin").css("background-color", "#D3DCE0");
        $("#Res").prop('disabled', true);
        $("#Res").disabled = true;
        $("#Res").css("color", "GrayText");
        $("#Res").css("background-color", "Gray");
        $("input[name = 'Gamefin']").val("fin");
        alert("You win!!!");
    }
}

$(window).bind('beforeunload', function () {
    if ($("input[name = 'Unloadbut']").val() != "Res" && $("input[name = 'Unloadbut']").val() != "Next") {
        if ($("input[name = 'Gamefin']").val() != "fin") {
            $.cookie('isbaseunengame', 'true', { expires: 7, path: '/' });
            var pararr = [{ ParName: "Par0", ParValue: "1" },
                      { ParName: "Par1", ParValue: "1" },
                      { ParName: "Par2", ParValue: "1" },
                      { ParName: "Par3", ParValue: "1" },
                      { ParName: "Par4", ParValue: "1" },
                      { ParName: "Par5", ParValue: "1" },
                      { ParName: "Par6", ParValue: "1" },
                      { ParName: "Par7", ParValue: "1" },
                      { ParName: "Par8", ParValue: "1" },
                      { ParName: "Par9", ParValue: "1" },
                      { ParName: "Par10", ParValue: "1" },
                      { ParName: "Par11", ParValue: "1" },
                      { ParName: "Par12", ParValue: "1" },
                      { ParName: "Par13", ParValue: "1" },
                      { ParName: "Par14", ParValue: "1" },
                      { ParName: "Par15", ParValue: "1" },
                      { ParName: "turns", ParValue: "1"}];
            for (var count = 0; count < 17; count++) {
                if (count < 16) {
                    if ($("input[name = 'butt" + count + "']").val() != "")
                        pararr[count].ParValue = $("input[name = 'butt" + count + "']").val();
                    else
                        pararr[count].ParValue = "0";
                }
                else
                    pararr[count].ParValue = $("#turns").val();
            }
            $.ajax({
                type: "POST",
                url: "/Game/Savegame",
                async: false,
                traditional: true,
                data: JSON.stringify(pararr),
                contentType: "application/json, charset=utf-8",
                dataType: "json",
                success: function (message) {
                    alert(message);
                }
            });
        }
        else {
            $.deletecookie;
        }
    }
});

function Reset() {
    $("input[name = 'Gamefin']").val("unfin");
    $("input[name = 'Unloadbut']").val("Res");
}

function Next() {
    $("input[name = 'Unloadbut']").val("Next");
}