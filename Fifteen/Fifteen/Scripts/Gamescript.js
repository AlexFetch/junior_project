﻿var fins, fin, gfi;
window.onload = function Load() {
    for (var i = 0; i < 16; i++) {
        fins = "butt" + i;
        fin = document.getElementsByName(fins);
        gfi = fin[0];
        if (gfi.value == "") {
            gfi.style.backgroundColor = "White";
            break;
        }
    }
    fins = document.getElementsByName("Gamefin");
    gfi = fins[0];
    gfi.value = "unfin";
}
function Pushed(e) {
    var pbl, pbt, wb, zeroc, zerol, zerot, move, pbv, zero, zerov, tbutt, zerotil, zeroti, zeroli, pbls, pbts, zerols, zerots, turns, turn, turnv;
    var e = window.event || e;
    var pb = e.target || e.srcElement;
    pbv = pb.value;
    for (var i = 0; i < 16; i++) {
        tbutt = "butt" + i;
        zerotil = document.getElementsByName(tbutt);
        zeroti = zerotil[0];
        zeroc = zeroti.style.backgroundColor;
        if (zeroc == "white" || zeroc == "White") {
            wb = "butt" + i;
            break;
        }
    }
    pbls = pb.style.left;
    pbl = pbls.slice(0, 3);
    pbts = pb.style.top;
    pbt = pbts.slice(0, 3);
    zeroli = document.getElementsByName(wb);
    zero = zeroli[0];
    zerov = zero.value;
    zerols = zero.style.left;
    zerol = zerols.slice(0, 3);
    zerots = zero.style.top;
    zerot = zerots.slice(0, 3);
    move = false;
    if (zerol - pbl == 50 && zerot == pbt) {
        move = true;
    }
    else
        if (zerol - pbl == -50 && zerot == pbt) {
            move = true;
        }
        else
            if (zerot - pbt == 50 && zerol == pbl) {
                move = true;
            }
            else
                if (zerot - pbt == -50 && zerol == pbl) {
                    move = true;
                };
    if (move == true) {
        pb.style.backgroundColor = "White";
        pb.value = zerov;
        zero.style.backgroundColor = "#9cf527";
        zero.value = pbv;
        turns = document.getElementsByName("Turns");
        turn = turns[0];
        turnv = turn.value;
        turnv++;
        turn.value = turnv;
    }
    Finish();
};

function Finish() {
    var butmas = [];
    var butt, top, left, val, selbut, selbuts;
    var win = false;
    var counti, countj, countk;
    for (var i = 1; i < 16; i++) {
        tbutt = "butt" + i;
        zerotil = document.getElementsByName(tbutt);
        zeroti = zerotil[0];
        if (zeroti.value != i) {
            break;
        }
        if (i == 15) {
            win = true;
        }
    }
    if (win == true) {
        fin = document.getElementById("Fin");
        fin.disabled = false;
        fin.style.color = "#333333";
        fin.style.backgroundColor = "#D3DCE0";
        var res = document.getElementById("Res");
        res.disabled = true;
        res.style.color = "GrayText";
        res.style.backgroundColor = "Gray";
        fins = document.getElementsByName("Gamefin");
        gfi = fins[0];
        gfi.value = "fin";
        alert("You win!!!");
    }
}

window.onbeforeunload = function () {
    fins = document.getElementsByName("Gamefin");
    gfi = fins[0];
    if (gfi.value != "fin") {
        document.cookie = "isbaseunengame=true; expires=01/00/2050 00:00:00; path=/";
        var pararr = [{ value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" },
                              { value: "1" }, ];
        var turnes = "";
        for (var i = 0; i < 16; i++) {
            tbutt = "butt" + i;
            zerotil = document.getElementsByName(tbutt);
            zeroti = zerotil[0];
            pararr[i].value = zeroti.value;
        }
        turnes = document.getElementById("turns").value;
        var sendreq = Createrequest();
        sendreq.onreadystatechange = function () {
            if (sendreq.readyState == 4 && sendreq.status == 200) {
                alert(sendreq.responseText);
            }
        }
        sendreq.open('POST', '/Game/Savegame', false);
        sendreq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        sendreq.send("button0=" + pararr[0].value + "&button1=" + pararr[1].value + "&button2=" + pararr[2].value + "&button3=" + pararr[3].value + "&button4=" + pararr[4].value + "&button5=" + pararr[5].value + "&button6=" + pararr[6].value + "&button7=" + pararr[7].value + "&button8=" + pararr[8].value + "&button9=" + pararr[9].value + "&button10=" + pararr[10].value + "&button11=" + pararr[11].value + "&button12=" + pararr[12].value + "&button13=" + pararr[13].value + "&button14=" + pararr[14].value + "&button15=" + pararr[15].value + "&turnes=" + turnes);
    }
    else {
        var cookiedate = new Date();
        cookiedate.setTime(cookiedate.getTime() - 1);
        document.cookie = "isbaseunengame=; expires=" + cookiedate.toGMTString();
    }
}

function Createrequest() {
    var request;
    try {
        request = new XMLHttpRequest();
    } catch (e) {
        try {
            request = new ActiveXObject('Microsoft.XMLHTTP');
        } catch (e) {
            try {
                request = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                request = null;
            }
        }
    }
    return request;
}

function Reset() {
    fins = document.getElementsByName("Gamefin");
    gfi = fins[0];
    gfi.value = "fin";
}