﻿$(document).ready(function () {
    var exists = cookieexists("isbaseunengame");
    if (exists) {
        $("#cont").prop('disabled', false);
        $("#cont").css("color", "#333333");
        $("#cont").css("background-color", "#D3DCE0");
    }
});

function cookieexists(cookiename) {
    var isexists = $.cookie(cookiename);
    if (isexists)
        return true;
    else
        return false;
}