﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Fifteen.Models
{
    public class LoginModel
    {
        [RequiredAttr]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [RequiredAttr]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }

    public class RegisterModel
    {
        [RequiredAttr]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [RequiredAttr]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [RequiredAttr]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [RequiredAttr]
        [StringLength(25, ErrorMessage = "The password must be at least 6 characters long and not more than 25 characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [RequiredAttr]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
