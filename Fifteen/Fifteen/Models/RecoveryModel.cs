﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Web;

namespace Fifteen.Models
{
    public class RecoveryModel
    {
        [RequiredAttr]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [RequiredAttr]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
    }
}