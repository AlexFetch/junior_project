﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Web;

namespace Fifteen.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Stat> UserStatistics { get; set; }
        public DbSet<Save> SavedGameData { get; set; }
    }

    [Table("Users")]
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    [Table("UserStatistics")]
    public class Stat
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public string Login { get; set; }
        public int EndGames { get; set; }
        public int UnendGames { get; set; }
        public int MinTurns { get; set; }
        public int MaxTurns { get; set; }
        public int AvgTurns { get; set; }
    }

    [Table("SavedGameData")]
    public class Save
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public string Login { get; set; }
        public string Button0 { get; set; }
        public string Button1 { get; set; }
        public string Button2 { get; set; }
        public string Button3 { get; set; }
        public string Button4 { get; set; }
        public string Button5 { get; set; }
        public string Button6 { get; set; }
        public string Button7 { get; set; }
        public string Button8 { get; set; }
        public string Button9 { get; set; }
        public string Button10 { get; set; }
        public string Button11 { get; set; }
        public string Button12 { get; set; }
        public string Button13 { get; set; }
        public string Button14 { get; set; }
        public string Button15 { get; set; }
        public string Turns { get; set; }
    }
}