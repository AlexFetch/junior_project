﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Fifteen.Filters;
using Fifteen.Models;

namespace Fifteen.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.Login, model.Password))
            {
                return RedirectToAction("UserProfile", "Account");
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                UsersContext _db = new UsersContext();
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Login, model.Password,
                        new { Name = model.Name, Surname = model.Surname, Password = model.Password });
                    _db.UserStatistics.Add(new Stat {Login = model.Login, EndGames = 0, UnendGames = 0, MinTurns = 0, AvgTurns = 0, MaxTurns = 0 });
                    _db.SavedGameData.Add(new Save {Login = model.Login, Button0 = "0", Button1 = "0", Button2 = "0", Button3 = "0", Button4 = "0", Button5 = "0", Button6 = "0", Button7 = "0", Button8 = "0", Button9 = "0", Button10 = "0", Button11 = "0", Button12 = "0", Button13 = "0", Button14 = "0", Button15 = "0", Turns = "0" });
                    _db.SaveChanges();
                    WebSecurity.Login(model.Login, model.Password);
                    return RedirectToAction("UserProfile", "Account");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View();
        }


        //
        // GET: /Account/Manage

        public ActionResult Manage(string message)
        {
            UsersContext _db = new UsersContext();
            try
            {
                ViewBag.Messagesucc = message;
                var query =
                from usersac in _db.Users
                where usersac.Login == User.Identity.Name
                select usersac;
                foreach (User usersac in query)
                {
                    ViewBag.Name = usersac.Name;
                    ViewBag.Surname = usersac.Surname;
                    ViewBag.Password = usersac.Password;
                }
            }
            catch (Exception)
            {
                ViewBag.Messagesucc = "";
                ViewBag.StatusMessageerr = "An error occured. Please try again later.";
                ViewBag.Name = "Error";
                ViewBag.Surname = "Error";
                ViewBag.Password = "Error";
            }
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ManageModel model)
        {
            UsersContext _db = new UsersContext();
            string uname = User.Identity.Name;
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded = false;
                    if (model.NewPassword != null)
                        {
                            try
                            {
                                changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                                var query =
                                from usersac in _db.Users
                                where usersac.Login == uname
                                select usersac;
                                foreach (User usersac in query)
                                {
                                    usersac.Password = model.NewPassword;
                                }
                                _db.SaveChanges();
                            }
                            catch (Exception)
                            {
                                changePasswordSucceeded = false;
                            }
                        }
                    if (model.Name != null)
                        {
                            try
                            {
                                var query =
                                    from usersac in _db.Users
                                    where usersac.Login == uname
                                    select usersac;
                                foreach (User usersac in query)
                                {
                                    usersac.Name = model.Name;
                                }
                                _db.SaveChanges();
                                changePasswordSucceeded = true;
                            }
                            catch (Exception)
                            {
                                changePasswordSucceeded = false;
                            }
                        }
                    if (model.Surname != null)
                        {
                            try
                            {
                                var query =
                                    from usersac in _db.Users
                                    where usersac.Login == uname
                                    select usersac;
                                foreach (User usersac in query)
                                {
                                    usersac.Surname = model.Surname;
                                }
                                _db.SaveChanges();
                                changePasswordSucceeded = true;
                            }
                            catch (Exception)
                            {
                                changePasswordSucceeded = false;
                            }
                        }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = "Your data has been changed." });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Error occured while updating your data. Please check entered data or try again later.");
                    }

                    try
                    {
                        var query =
                        from usersac in _db.Users
                        where usersac.Login == User.Identity.Name
                        select usersac;
                        foreach (User usersac in query)
                        {
                            ViewBag.Name = usersac.Name;
                            ViewBag.Surname = usersac.Surname;
                            ViewBag.Password = usersac.Password;
                        }
                    }
                    catch (Exception)
                    {
                        ViewBag.Messagesucc = "";
                        ViewBag.StatusMessageerr = "An error occured. Please try again later.";
                        ViewBag.Name = "Error";
                        ViewBag.Surname = "Error";
                        ViewBag.Password = "Error";
                    }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult UserProfile()
        {
            UsersContext _db = new UsersContext();
            string uname = User.Identity.Name;
            var queryac =
            from usersac in _db.Users
            where usersac.Login == uname
            select usersac;
            foreach (User usersac in queryac)
            {
                ViewBag.Name = usersac.Name;
                ViewBag.Surname = usersac.Surname;
                ViewBag.Password = usersac.Password;
            };
            var queryst =
            from userstat in _db.UserStatistics
            where userstat.Login == uname
            select userstat;
            foreach (Stat userstat in queryst)
            {
                ViewBag.EndGames = userstat.EndGames;
                ViewBag.UnendGames = userstat.UnendGames;
                ViewBag.MinTurns = userstat.MinTurns;
                ViewBag.MaxTurns = userstat.MaxTurns;
                ViewBag.AvgTurns = userstat.AvgTurns;
            }
            return View();
        }

        [HttpPost]
        public ActionResult UserProfile(string profbtn)
        {
            var cookie = Request.Cookies["isbaseunengame"];
            if (cookie != null && profbtn == "New game")
            {
                UsersContext _db = new UsersContext();
                var query =
                from userstat in _db.UserStatistics
                where userstat.Login == User.Identity.Name
                select userstat;
                foreach (Stat userstat in query)
                {
                    userstat.UnendGames++;
                }
                var querydel =
                from usersave in _db.SavedGameData
                where usersave.Login == User.Identity.Name
                select usersave;
                foreach (Save usersave in querydel)
                {
                    usersave.Button0 = "0";
                    usersave.Button1 = "0";
                    usersave.Button2 = "0";
                    usersave.Button3 = "0";
                    usersave.Button4 = "0";
                    usersave.Button5 = "0";
                    usersave.Button6 = "0";
                    usersave.Button7 = "0";
                    usersave.Button8 = "0";
                    usersave.Button9 = "0";
                    usersave.Button10 = "0";
                    usersave.Button11 = "0";
                    usersave.Button12 = "0";
                    usersave.Button13 = "0";
                    usersave.Button14 = "0";
                    usersave.Button15 = "0";
                    usersave.Turns = "0";
                }
                _db.SaveChanges();
                Response.Cookies["isbaseunengame"].Expires = DateTime.Now.AddDays(-1);
            }
            switch (profbtn)
            {
                case "Change data": return RedirectToAction("Manage", "Account");
                case "New game": return RedirectToAction("Pushed", "Game");
                case "Continue game": return RedirectToAction("Pushed", "Game", new { Message = "Continue" });
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Recover(string message)
        {
            if (message != null)
            {
                ViewBag.Messageone = message;
                ViewBag.Messagetwo = "You can change this password later on \"Change data\" page.";
            }
            else
            {
                ViewBag.Messageone = "";
                ViewBag.Messagetwo = "";
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Recover(RecoveryModel model)
        {
            UsersContext _db = new UsersContext();
            bool recoverPasswordSucceeded = false;
            int milisec;
            string newpass = "", restoken, succmessage = "";
            try
            {
                var query =
                from usersac in _db.Users
                where usersac.Login == model.Login
                select usersac;
                recoverPasswordSucceeded = true;
                foreach (User usersac in query)
                {
                    if (usersac.Surname != model.Surname)
                        throw new Exception();
                }
                milisec = DateTime.Now.Millisecond;
                Random rnd1 = new Random(milisec);
                newpass = Convert.ToString(rnd1.Next(100000, 999999));
                restoken = WebSecurity.GeneratePasswordResetToken(model.Login);
                recoverPasswordSucceeded = WebSecurity.ResetPassword(restoken, newpass);
                var queryps =
                from usersac in _db.Users
                where usersac.Login == model.Login
                select usersac;
                foreach (User usersac in query)
                {
                    usersac.Password = newpass;
                }
                _db.SaveChanges();
            }
            catch (Exception)
            {
                recoverPasswordSucceeded = false;
            }
            if (recoverPasswordSucceeded == true)
            {
                succmessage = "Password recovery succeeded. Your temporary password is " + newpass + ".";
                return RedirectToAction("Recover", new { Message = succmessage });
            }
            else
            {
                ModelState.AddModelError("", "The entered login or surname does not exists.");
            }
            
            return View();
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
