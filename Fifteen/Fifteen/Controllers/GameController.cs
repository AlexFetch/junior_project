﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Fifteen.Filters;
using Fifteen.Models;
using Fifteen.Viewmodels;

namespace Fifteen.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class GameController : Controller
    {
        //
        // GET: /Game/

        public ActionResult Pushed(string message)
        {
            if (message == "Continue")
            {
                UsersContext _db = new UsersContext();
                var query =
                from usersave in _db.SavedGameData
                where usersave.Login == User.Identity.Name
                select usersave;
                foreach (Save usersave in query)
                {
                    ViewBag.Value0 = usersave.Button0;
                    ViewBag.Value1 = usersave.Button1;
                    ViewBag.Value2 = usersave.Button2;
                    ViewBag.Value3 = usersave.Button3;
                    ViewBag.Value4 = usersave.Button4;
                    ViewBag.Value5 = usersave.Button5;
                    ViewBag.Value6 = usersave.Button6;
                    ViewBag.Value7 = usersave.Button7;
                    ViewBag.Value8 = usersave.Button8;
                    ViewBag.Value9 = usersave.Button9;
                    ViewBag.Value10 = usersave.Button10;
                    ViewBag.Value11 = usersave.Button11;
                    ViewBag.Value12 = usersave.Button12;
                    ViewBag.Value13 = usersave.Button13;
                    ViewBag.Value14 = usersave.Button14;
                    ViewBag.Value15 = usersave.Button15;
                    ViewBag.Turns = usersave.Turns;
                }
            }
            else
            {
                var cubes = new List<CubeModel> 
                {
                new CubeModel() { Value = "1" },
                new CubeModel() { Value = "2" },
                new CubeModel() { Value = "3" },
                new CubeModel() { Value = "4" },
                new CubeModel() { Value = "5" },
                new CubeModel() { Value = "6" },
                new CubeModel() { Value = "7" },
                new CubeModel() { Value = "8" },
                new CubeModel() { Value = "9" },
                new CubeModel() { Value = "10" },
                new CubeModel() { Value = "11" },
                new CubeModel() { Value = "12" },
                new CubeModel() { Value = "13" },
                new CubeModel() { Value = "14" },
                new CubeModel() { Value = "15" }
                };
                var timecub = new List<CubeModel> { null };
                int[] mixarray = new int[15];
                int i = 0, j = 0, milisec, timeel;
                milisec = DateTime.Now.Millisecond;
                Random rnd = new Random(milisec);
                for (i = 0; i < 15; i++)
                {
                    mixarray[i] = rnd.Next(0, 2014);
                }
                for (i = 0; i < 14; i++)
                {
                    for (j = i + 1; j < 15; j++)
                    {
                        if (mixarray[i] > mixarray[j])
                        {
                            timeel = mixarray[i];
                            mixarray[i] = mixarray[j];
                            mixarray[j] = timeel;
                            timecub[0] = cubes[i];
                            cubes[i] = cubes[j];
                            cubes[j] = timecub[0];
                        }
                    }
                }
                for (int count = 0; count < 15; count++)
                {
                    switch (count)
                    {
                        case 0: ViewBag.Value1 = cubes[count].Value; break;
                        case 1: ViewBag.Value2 = cubes[count].Value; break;
                        case 2: ViewBag.Value3 = cubes[count].Value; break;
                        case 3: ViewBag.Value4 = cubes[count].Value; break;
                        case 4: ViewBag.Value5 = cubes[count].Value; break;
                        case 5: ViewBag.Value6 = cubes[count].Value; break;
                        case 6: ViewBag.Value7 = cubes[count].Value; break;
                        case 7: ViewBag.Value8 = cubes[count].Value; break;
                        case 8: ViewBag.Value9 = cubes[count].Value; break;
                        case 9: ViewBag.Value10 = cubes[count].Value; break;
                        case 10: ViewBag.Value11 = cubes[count].Value; break;
                        case 11: ViewBag.Value12 = cubes[count].Value; break;
                        case 12: ViewBag.Value13 = cubes[count].Value; break;
                        case 13: ViewBag.Value14 = cubes[count].Value; break;
                        case 14: ViewBag.Value15 = cubes[count].Value; break;
                    }
                }
                ViewBag.Value0 = "0";
                ViewBag.Turns = "0";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Pushed(string finres, string turns)
        {
            if (finres == "Reset")
            {
                return RedirectToAction("Pushed");
            }
            else
                if (finres == "Next")
                {
                    var cookie = Request.Cookies["isbaseunengame"];
                    UsersContext _db = new UsersContext();
                    int turnsin = Convert.ToInt32(turns), maxt, mint, avgt;
                    var query =
                    from userstat in _db.UserStatistics
                    where userstat.Login == User.Identity.Name
                    select userstat;
                    foreach (Stat usersac in query)
                    {
                       usersac.EndGames++;
                       if (usersac.MinTurns == 0 && usersac.MaxTurns == 0)
                       {
                           usersac.MinTurns = turnsin;
                           usersac.MaxTurns = turnsin;
                           usersac.AvgTurns = (turnsin + turnsin) / 2;
                       }
                       else
                       {
                           mint = usersac.MinTurns;
                           maxt = usersac.MaxTurns;
                           if (turnsin < mint)
                           {
                               mint = turnsin;
                               avgt = (mint + maxt) / 2;
                               usersac.MinTurns = mint;
                               usersac.MaxTurns = maxt;
                               usersac.AvgTurns = avgt;
                           }
                           else
                               if (turnsin > maxt)
                               {
                                   maxt = turnsin;
                                   avgt = (mint + maxt) / 2;
                                   usersac.MinTurns = mint;
                                   usersac.MaxTurns = maxt;
                                   usersac.AvgTurns = avgt;
                               }
                       }
                    }
                    _db.SaveChanges();
                    if (cookie != null)
                    {
                        Response.Cookies["isbaseunengame"].Expires = DateTime.Now.AddDays(-1);
                    }
                    return RedirectToAction("UserProfile", "Account");
                }
            return View();
        }

        [HttpPost]
        public JsonResult Savegame(SavePars[] pararr)
        {
            UsersContext _db = new UsersContext();
            bool gamesavesuccessful = true;
            string message = "";
            int count = 0;
            try
            {
                var query =
                from usersave in _db.SavedGameData
                where usersave.Login == User.Identity.Name
                select usersave;
                foreach (Save usersave in query)
                {
                    foreach (SavePars kv in pararr)
                    {
                        switch (count)
                        {
                            case 0: usersave.Button0 = kv.ParValue; break;
                            case 1: usersave.Button1 = kv.ParValue; break;
                            case 2: usersave.Button2 = kv.ParValue; break;
                            case 3: usersave.Button3 = kv.ParValue; break;
                            case 4: usersave.Button4 = kv.ParValue; break;
                            case 5: usersave.Button5 = kv.ParValue; break;
                            case 6: usersave.Button6 = kv.ParValue; break;
                            case 7: usersave.Button7 = kv.ParValue; break;
                            case 8: usersave.Button8 = kv.ParValue; break;
                            case 9: usersave.Button9 = kv.ParValue; break;
                            case 10: usersave.Button10 = kv.ParValue; break;
                            case 11: usersave.Button11 = kv.ParValue; break;
                            case 12: usersave.Button12 = kv.ParValue; break;
                            case 13: usersave.Button13 = kv.ParValue; break;
                            case 14: usersave.Button14 = kv.ParValue; break;
                            case 15: usersave.Button15 = kv.ParValue; break;
                            case 16: usersave.Turns = kv.ParValue; break;
                        }
                        count++;
                    }

                }
                _db.SaveChanges();
            }
            catch (Exception)
            {
                gamesavesuccessful = false;
            }
            if (gamesavesuccessful == true)
            {
                message = "Game saved!";
                return Json(message);
            }
            else
            {
                message = "Error occured, game don't saved.";
                return Json(message);
            }
        }
    }
}
